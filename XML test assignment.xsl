<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template match="/">
<!-- Good work here too, Ola.  Your xsl properly formats and outputs the xml data
I saw the comment.  :-)
10/10
-->
	<html>
		<head>
			<title>Customer information</title>
		</head>
		<body>
			<p>Customer Info</p>
			<br/>Name: <xsl:value-of select="telephoneBill/customer/name"/>
			<br/>Address: <xsl:value-of select="telephoneBill/customer/address"/>
			<br/>City: <xsl:value-of select="telephoneBill/customer/city"/>
			<br/>Province: <xsl:value-of select="telephoneBill/customer/province"/>
			
			<br/>
			<!--display information in table-->
			<table border="1">
				<tbody>
					<tr>
						<th>Called Number</th>
						<th>Date</th>
						<th>Duration in Minutes</th>
						<th>Charge</th>
					</tr>
					<!--loop through the call elements and display-->
					<xsl:for-each select="telephoneBill/call">
						<tr>
							<td><xsl:value-of select="@number"/></td>
							<td><xsl:value-of select="@date"/></td>
							<td><xsl:value-of select="@durationInMinutes"/></td>
							<td><xsl:value-of select="@charge"/></td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</body>
	</html>
</xsl:template>
</xsl:stylesheet>
